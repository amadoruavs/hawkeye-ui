import sys
from PyQt5 import QtGui, QtCore, QtWidgets
from PyQt5.QtCore import Qt, QPoint, QRect, QSize
from PyQt5.QtGui import QScreen
from PyQt5.QtWidgets import QApplication, QLabel, QRubberBand, QAction
from PyQt5.QtGui import *


class KpeWindow(QtWidgets.QLabel):
    def __init__(self, parent=None):
        QtWidgets.QLabel.__init__(self,parent)
        main = QtWidgets.QVBoxLayout(self)
        self.selection = QtWidgets.QRubberBand(QtWidgets.QRubberBand.Rectangle, self)
        self.screen = None

    def shoot(self):
        print("Grabbing", self.lower_right, self.lower_left, self.upper_left, self.lower_left)
        x = self.lower_right - self.lower_left
        y = self.upper_left - self.lower_left
        print(int(self.winId()))
        print(x,y)
        p = self.grab(self.selection.rect())
        p.save("yeet.jpg", 'jpg')
        print ("shot taken")

    def keyPressEvent(self, event):
        self.parent().keyPressEvent(event)

    def mousePressEvent(self, event):
        '''
            Mouse is pressed. If selection is visible either set dragging mode (if close to border) or hide selection.
            If selection is not visible make it visible and start at this point.
        '''
        print("pressEvent")
        if event.button() == QtCore.Qt.LeftButton:

            position = QtCore.QPoint(event.pos())
            if self.selection.isVisible():
                print(self.upper_left, self.upper_right, self.lower_left, self.lower_right)
                print("edit selection")
                # visible selection
                if (self.upper_left - position).manhattanLength() < 20:
                    # close to upper left corner, drag it
                    self.mode = "drag_upper_left"
                elif (self.lower_right - position).manhattanLength() < 20:
                    # close to lower right corner, drag it
                    self.mode = "drag_lower_right"
                elif (self.lower_left - position).manhattanLength() <20:
                    self.mode = "drag_lower_left"
                elif (self.upper_right- position).manhattanLength() <20:
                    self.mode = "drag_upper_right"
                else:
                    # clicked somewhere else, hide selection
                    print("shoot")
                    self.shoot()
                    self.selection.hide()
            else:
                # no visible selection, start new selection
                print("new selection")
                self.upper_left = position
                self.lower_left = position
                self.upper_right = position
                self.lower_right = position 
                self.mode = "drag_lower_right"
                self.selection.show()
                print(self.upper_left, self.upper_right, self.lower_left, self.lower_right)

    def mouseMoveEvent(self, event):
        '''
            Mouse moved. If selection is visible, drag it according to drag mode.
        '''

        if self.selection.isVisible():
            # visible selection
            if self.mode is "drag_lower_right":
                self.lower_right = QtCore.QPoint(event.pos())
            elif self.mode is "drag_upper_left":
                self.upper_left = QtCore.QPoint(event.pos())
            # update geometry
            elif self.mode is "drag_lower_left":
                self.lower_left = QtCore.QPoint(event.pos())
            elif self.mode is "drag_upper_right":
                self.upper_right = QtCore.QPoint(event.pos())
            self.selection.setGeometry(QtCore.QRect(self.upper_left, self.lower_right).normalized())


class MainWindow(QtWidgets.QWidget):
    def __init__(self):
        super(MainWindow, self).__init__()
        print(int(self.winId()))
        QtWidgets.QMainWindow.__init__(self)

        layout = QtWidgets.QVBoxLayout(self)

        self.label = KpeWindow(self)
        pixmap = QScreen.grabWindow(app.primaryScreen(), app.desktop().winId())
        self.label.setPixmap(pixmap)
        layout.addWidget(self.label)

        self.label.setFocusPolicy(Qt.StrongFocus)
        self.setFocusProxy(self.label)
        self.label.setFocus(True)

        self.setLayout(layout)

        geometry = app.desktop().availableGeometry()

        self.setFixedSize(geometry.width(), geometry.height())

        self.show()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    mainWin = MainWindow()
    mainWin.label.screen = QApplication.desktop()
    mainWin.show()
    sys.exit(app.exec_())

